# ArenaShooter

Crimsonland-like arena shooter.

Weapons:

- Rifle
- Shotgun
- Coilgun
- Rocketlauncher

Movement: WSAD

Fire: LMB
Secondary fire: RMB

Upgrade shop: R

Secodary fire only ready on coilgun.
Press RMB to accumulate shot power.
Release to shoot.

Not completed:
Rifle secondary (burst fire)
Shotgun secondary (double shot)
Rocketlauncher secondary (Rockets follow mouse cursor)

Video gameplay demonstration:
https://drive.google.com/file/d/1tChcPDnz0UhuzlyJLe7txJtecLF0EDoD/

Developed with Unreal Engine 4
